<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\User ;



class UsersTablesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create([
            'firstname'=>'Jhon',
            'name' => 'Jhon Nesh',
            'email'=>"jhon_nesh@gmail.com",
            'password'=>bcrypt('password'),
            'remember_token'=>str_random(10),
        ]);
    }

}
