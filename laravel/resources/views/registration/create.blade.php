<!DOCTYPE html>
<html>
 <head>
  <title>Simple Login System in Laravel</title>
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" />
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
  <style type="text/css">
   .box{
    width:600px;
    margin:0 auto;
    border:1px solid #ccc;
   }
  </style>
 </head>
 <body>
  <br />
  <div class="container box">
   <h3 align="center">Simple Register System in Laravel</h3><br />
   <form method="POST" action="/register">
        {{ csrf_field() }}
        <div class="form-group">
            <label for="firstname">FirstName:</label>
            <input type="text" class="form-control" id="firstname" name="firstname">
        </div>
        <div class="form-group">
            <label for="name">Name:</label>
            <input type="text" class="form-control" id="name" name="name">
        </div>
      
        <div class="form-group">
            <label for="email">Email:</label>
            <input type="email" class="form-control" id="email" name="email">
        </div>
 
        <div class="form-group">
            <label for="password">Password:</label>
            <input type="password" class="form-control" id="password" name="password">
        </div>
 <div class="form-group{{ $errors->has('terms') ? ' has-error' : '' }}">
  <label>
  <input type="checkbox" class="form-control" name="terms" value="1" /> Agree with the terms and conditions
  </label>

  <div class="col-md-4">
    @if ($errors->has('terms'))
     <span class="help-block">
       <strong>{{ $errors->first('terms') }}</strong>
     </span>
    @endif
  </div>
</div>
        <div class="form-group">
            <button style="cursor:pointer" type="submit" class="btn btn-primary">Submit</button>
        </div>
    </form>
  </div>
 </body>
</html>
