<?php

namespace App\Http\Controllers;
Use App\Models\User;
use Illuminate\Http\Request;

class RegistrationController extends Controller
{
    public function create()
    {
        return view('registration.create');
    }
    public function store()
    {
        $this->validate(request(), [
            'firstname'=>'required',
            'name' => 'required',
            'email' => 'required|email',
            'password' => 'required',
            'terms' => 'required'

        ]);
       
        $user = User::create(request(['firstname' ,'name', 'email', 'password']));
       
       // auth()->login($user);
        
        return redirect()->to('/main');
    }
}